﻿#include <iostream>
class Animal
{
public:
    virtual void makeSound() const = 0;
};

class Cat : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Meaou!\n";
    }
};

class Dog : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Woof!\n";
    }
};

class Mouse : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Squeak-squeak\n";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Mouse();

    for (Animal* a : animals)
        a->makeSound();
}
